/src/main/resources/application.properties
- Update DB information (url, username, password)
- Update security information (client-id, client-secret. Default is: testjwtclientid/password)
==========================================================================================
- DB schema will be auto generated after starting.
- Init data by sql: /sql-scripts/data.sql 
- Have 3 default users: user, admin, mod. All users have default password: "password"
- We can change default password with pattern: 
	{noop}[PASSWORD] 
	{bcrypt}[ENCRYPTED_PASSWORD_WITH_BCRYPT]
- We can use [POST] /private/user/add (with role admin) to insert user 
	Sample Request: 
	{
		"name": "Test",
		"start_date": "2018-01-01T00:00:00.000+0000"
	}
