package vuong.springboot.assignment;

import java.util.Calendar;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import vuong.springboot.assignment.entity.User;
import vuong.springboot.assignment.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AssignmentApplicationTests {

	@Autowired
	private UserRepository userRepository;

	@Test
	public void whenGetAll_thenReturnUser() {
		// given
		List<User> current = userRepository.findAll();
		int currentCount = (current == null || current.isEmpty()) ? 0 : current.size();
		
		User user = new User();
		user.setName("Adam");
		user.setStart_date(Calendar.getInstance().getTime());
		userRepository.save(user);

		// when
		List<User> found = userRepository.findAll();

		// then
		assertThat(found.size()).isEqualTo(currentCount + 1);

		User userFound = found.get(0);

		assertThat(userFound.getName()).isEqualTo(user.getName());
	}
}
