package vuong.springboot.assignment;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import vuong.springboot.assignment.dto.UserDTO;
import vuong.springboot.assignment.entity.User;
import vuong.springboot.assignment.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestPropertySource("classpath:application.properties")
@SqlGroup({ @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:data.sql") })
public class RestUserControllerTest {

	private static final String TOKEN_PARAM = "?grant_type=password&username=%s&password=%s";

	private static final String URL_OAUTH_TOKEN = "/oauth/token";

	private static final String BASE_URL = "http://localhost:9999";

	private static final String URL_USER_DETAIL = "/private/user/detail/";

	private static final String URL__GET_START_DATE = "/private/user/getStartDate/";

	private static final String URL_GET_NAME = "/private/user/getName/";

	private static final int TESTING_USER_ID = 5;

	@Autowired
	private UserService userService;
	
	@Autowired
	private WebTestClient webTestClient;

	@Before
	public void setup() {
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testHealthCheck() {
		webTestClient
				// Create a GET request to test an endpoint
				.get().uri("/actuator/health").accept(MediaType.APPLICATION_JSON).exchange()
				// and use the dedicated DSL to test assertions against the
				// response
				.expectStatus().isOk()
				.expectBody().jsonPath("$.status").isEqualTo("UP");
	}

	@Test
	public void testGetDataWithToken() {
		String token = getAccessToken("admin", "password");
		assertThat(token).isNotNull();

		// All request will be performed by Admin
		
		// Verify response when get name
		// Create a GET request to test an endpoint
		webTestClient
				.get().uri(URL_GET_NAME + TESTING_USER_ID)
				.header("Authorization", "Bearer " + token)
				.header("Content-Type", "application/json")
				.exchange().expectStatus().isOk()
				.expectBody()
				.jsonPath("$.name").isEqualTo("Test");
		// Verify response when get start_date
		// Create a GET request to test an endpoint
		webTestClient
				.get().uri(URL__GET_START_DATE + TESTING_USER_ID)
				.header("Authorization", "Bearer " + token)
				.header("Content-Type", "application/json")
				.exchange()
				.expectStatus().isOk()
				.expectBody().jsonPath("$.start_date").hasJsonPath();
		// Verify response when get all detail
		// Create a GET request to test an endpoint
		webTestClient
				.get().uri(URL_USER_DETAIL + TESTING_USER_ID)
				.header("Authorization", "Bearer " + token)
				.header("Content-Type", "application/json").exchange()
				.expectStatus().isOk()
				.expectBody()
				.jsonPath("$.name").isEqualTo("Test")
				.jsonPath("$.id").isEqualTo(TESTING_USER_ID)
				.jsonPath("$.loyal_point").hasJsonPath()
				.jsonPath("$.start_date").hasJsonPath();
	}
	
	@Test
	public void testLoyalPoint() throws IOException, JSONException {
		OkHttpClient client = new OkHttpClient();		
		String token = getAccessToken("admin", "password");
		Request request = new Request.Builder()
				.url(BASE_URL + URL_USER_DETAIL + TESTING_USER_ID)
				.get()
				.addHeader("Content-Type", "application/json")
				.addHeader("Authorization", "Bearer " + token).build();
		Response response = client.newCall(request).execute();

		JSONObject jsonObject = new JSONObject(response.body().string());
		
		User user = userService.getById(TESTING_USER_ID);
		long expectedPoint = (TestUtil.getDiffDays(user.getStart_date(), Calendar.getInstance().getTime()) + 1) * 5;

		long point = jsonObject.getLong("loyal_point");

		// Verify loyal_point from REST
		assertThat(point).isEqualTo(expectedPoint);
		
		UserDTO userDTO = userService.getUserDTODetails(TESTING_USER_ID);
		
		// Verify loyal_point from UserService
		assertThat(userDTO.getLoyal_point()).isEqualTo(expectedPoint);
	}

	// Get access token
	private String getAccessToken(String username, String password) {
		try {
			OkHttpClient client = new OkHttpClient();
			okhttp3.MediaType mediaType = okhttp3.MediaType.parse("application/octet-stream");
			RequestBody body = RequestBody.create(mediaType, "{}");
			
			Request request = new Request.Builder()
					.url(BASE_URL + URL_OAUTH_TOKEN + String.format(TOKEN_PARAM, username, password))
					.post(body)
					.addHeader("Content-Type", "application/json")
					.addHeader("Authorization", "Basic dGVzdGp3dGNsaWVudGlkOnBhc3N3b3Jk").build();
			
			Response response = client.newCall(request).execute();
			
			JSONObject jsonObject = new JSONObject(response.body().string());
			return jsonObject.getString("access_token");
		} catch (Exception ex) {
			return null;
		}
	}
}
