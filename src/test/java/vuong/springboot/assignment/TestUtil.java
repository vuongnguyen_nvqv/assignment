package vuong.springboot.assignment;

import java.util.Date;

import org.springframework.test.web.reactive.server.WebTestClient;

import vuong.springboot.assignment.service.UserService;

public class TestUtil {
	public static long getDiffDays(Date start, Date end) {
		Date startDate = new Date(start.getYear(), start.getMonth(), start.getDate());
		Date endDate = new Date(end.getYear(), end.getMonth(), end.getDate());
		return (endDate.getTime() - startDate.getTime()) / (24 * 60 * 60 * 1000);
	}
}