package vuong.springboot.assignment;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;

import vuong.springboot.assignment.dto.UserDTO;
import vuong.springboot.assignment.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource("classpath:application.properties")
@SqlGroup({ @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:data.sql") })
public class UserServiceTests {
	@Autowired
	UserService userService;

	@Test
	public void testGetLoyalPoint() {
		UserDTO user = userService.getUserDTODetails(5);
		assertThat(user).isNotNull();

		long expectedPoint = (TestUtil.getDiffDays(user.getStart_date(), Calendar.getInstance().getTime()) + 1) * 5;

		assertThat(user.getLoyal_point()).isEqualTo(expectedPoint);
	}
}
