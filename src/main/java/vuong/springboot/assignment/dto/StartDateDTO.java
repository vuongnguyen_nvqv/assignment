package vuong.springboot.assignment.dto;

import java.util.Date;

public class StartDateDTO implements IClientResponse {
	private Date start_date;

	public StartDateDTO(Date start_date) {
		super();
		this.start_date = start_date;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
}