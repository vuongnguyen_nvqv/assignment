package vuong.springboot.assignment.dto;

import java.util.Calendar;
import java.util.Date;

import vuong.springboot.assignment.entity.User;

public class UserDTO implements IClientResponse {
	private long id;
	private String name;
	private Date start_date;
	private long loyal_point;

	public UserDTO() {
		super();
	}

	public UserDTO(User user) {
		super();
		this.id = user.getId();
		this.name = user.getName();
		this.start_date = user.getStart_date();
		this.loyal_point = (getDiffDays(user.getStart_date(), Calendar.getInstance().getTime()) + 1) * 5;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public long getLoyal_point() {
		return loyal_point;
	}

	public void setLoyal_point(long loyal_point) {
		this.loyal_point = loyal_point;
	}

	private long getDiffDays(Date start, Date end) {
		Date startDate = new Date(start.getYear(), start.getMonth(), start.getDate());
		Date endDate = new Date(end.getYear(), end.getMonth(), end.getDate());
		return (endDate.getTime() - startDate.getTime()) / (24 * 60 * 60 * 1000);
	}
}
