package vuong.springboot.assignment.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import vuong.springboot.assignment.entity.User;

@Repository
@Transactional(readOnly = true)
public class UserCustomRepositoryImpl implements UserCustomRepository {

	@PersistenceContext
	EntityManager entityManager;

	@Override
	public List<User> getFirstNamesLike(String name) {
		Query query = entityManager.createNativeQuery("SELECT u.* FROM user as u " + "WHERE u.name LIKE ?", User.class);
		query.setParameter(1, "%" + name + "%");
		return query.getResultList();
	}
}
