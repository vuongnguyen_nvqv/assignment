package vuong.springboot.assignment.repository;

import java.util.List;

import vuong.springboot.assignment.entity.User;

public interface UserCustomRepository {
    List<User> getFirstNamesLike(String name);
}