package vuong.springboot.assignment.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import vuong.springboot.assignment.entity.User;

public interface UserRepository extends CrudRepository<User,Long>, UserCustomRepository {
	List<User> findByName(String name);
	List<User> findAll();
}