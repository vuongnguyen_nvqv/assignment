package vuong.springboot.assignment.repository;

import org.springframework.data.repository.CrudRepository;

import vuong.springboot.assignment.security.entity.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {
}