package vuong.springboot.assignment.repository;

import org.springframework.data.repository.CrudRepository;

import vuong.springboot.assignment.entity.RandomCity;

public interface RandomCityRepository extends CrudRepository<RandomCity, Long> {
}