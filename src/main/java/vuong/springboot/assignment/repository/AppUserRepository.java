package vuong.springboot.assignment.repository;

import org.springframework.data.repository.CrudRepository;

import vuong.springboot.assignment.security.entity.AppUser;

public interface AppUserRepository extends CrudRepository<AppUser, Long> {
	AppUser findByUsername(String username);
}