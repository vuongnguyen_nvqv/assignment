package vuong.springboot.assignment.service;

import java.util.List;

import vuong.springboot.assignment.entity.RandomCity;
import vuong.springboot.assignment.security.entity.AppUser;

public interface GenericService {
	AppUser findByUsername(String username);

	List<AppUser> findAllUsers();

	List<RandomCity> findAllRandomCities();
}