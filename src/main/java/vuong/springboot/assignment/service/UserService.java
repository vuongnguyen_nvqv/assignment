package vuong.springboot.assignment.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vuong.springboot.assignment.dto.StartDateDTO;
import vuong.springboot.assignment.dto.UserDTO;
import vuong.springboot.assignment.dto.UserNameDTO;
import vuong.springboot.assignment.entity.User;
import vuong.springboot.assignment.repository.UserRepository;

@Component
public class UserService {
	@Autowired
	UserRepository userRepository;

	public User save(User user) {
		return userRepository.save(user);
	}

	public List<User> getAll() {
		return userRepository.findAll();
	}

	public List<User> findByName(String name) {
		return userRepository.getFirstNamesLike(name);
	}

	public User getById(long id) {
		Optional<User> userOpt = userRepository.findById(id);
		if (userOpt.isPresent()) {
			return userOpt.get();
		} else {
			return null;
		}
	}

	public UserNameDTO getNameById(long id) {
		User user = getById(id);
		if (user != null) {
			return new UserNameDTO(user.getName());
		}
		return null;
	}

	public StartDateDTO getStartDateById(long id) {
		User user = getById(id);
		if (user != null) {
			return new StartDateDTO(user.getStart_date());
		}
		return null;
	}

	public UserDTO getUserDTODetails(long id) {
		User user = getById(id);
		if (user != null) {
			return new UserDTO(user);
		}
		return null;
	}
}
