package vuong.springboot.assignment.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vuong.springboot.assignment.entity.RandomCity;
import vuong.springboot.assignment.repository.RandomCityRepository;
import vuong.springboot.assignment.repository.AppUserRepository;
import vuong.springboot.assignment.security.entity.AppUser;
import vuong.springboot.assignment.service.GenericService;

@Service
public class GenericServiceImpl implements GenericService {
    @Autowired
    private AppUserRepository userRepository;

    @Autowired
    private RandomCityRepository randomCityRepository;

    @Override
    public AppUser findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<AppUser> findAllUsers() {
        return (List<AppUser>)userRepository.findAll();
    }

    @Override
    public List<RandomCity> findAllRandomCities() {
        return (List<RandomCity>)randomCityRepository.findAll();
    }
}