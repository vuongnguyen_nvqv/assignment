package vuong.springboot.assignment.rest;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vuong.springboot.assignment.dto.IClientResponse;
import vuong.springboot.assignment.entity.User;
import vuong.springboot.assignment.service.UserService;

@RestController
@RequestMapping("/private/user")
public class UserController {
	@Autowired
	private UserService userService;

	/**
	 * Adding user. Admin only
	 * @param user
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/add")
	public User insert(@RequestBody User user) {
		return userService.save(user);
	}

/*	@RequestMapping(method = RequestMethod.GET, value = "/detail/{id}")
	public User getUser(@PathVariable long id) {
		return userService.getById(id);
	}*/

/*	@RequestMapping(method = RequestMethod.GET, value = "/detail")
	public List<User> getUserByName(@RequestParam(value = "name", defaultValue = "vuong") String name) {
		return userService.findByName(name);
	}
*/
/*	@RequestMapping(method = RequestMethod.GET)
	public List<User> getAllUser() {
		return userService.getAll();
	}*/

	/**
	 * Get userName by id. All users can use
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/getName/{id}")
	public IClientResponse getUserName(@PathVariable long id) {
		return userService.getNameById(id);
	}

	/**
	 * Get startDate by id. Admin & Mod can use
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/getStartDate/{id}")
	public IClientResponse getUserStartDate(@PathVariable long id) {
		return userService.getStartDateById(id);
	}

	/**
	 * Get all detail by id. Only admin can use
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/detail/{id}")
	public IClientResponse getUserDetail(@PathVariable long id) {
		return userService.getUserDTODetails(id);
	}
}