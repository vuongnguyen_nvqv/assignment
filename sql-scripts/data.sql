INSERT INTO app_role (id, role_name, description) VALUES (1, 'ROLE_USER', 'Standard User - Has no admin rights');
INSERT INTO app_role (id, role_name, description) VALUES (2, 'ROLE_ADMIN', 'Admin User - Has permission to perform admin tasks');
INSERT INTO app_role (id, role_name, description) VALUES (3, 'ROLE_MOD', 'Mod User - Has permission to perform mod tasks');

-- USER
-- non-encrypted password: jwtpass
INSERT INTO app_user (id, first_name, last_name, password, username) VALUES (1, 'User', 'user', '{bcrypt}$2a$11$nprb0swJhHitcdRk1N/dRurfZYKneFZ9PGaLW5Np8xNotPlJQ0hTq', 'user');
INSERT INTO app_user (id, first_name, last_name, password, username) VALUES (2, 'Admin', 'Admin', '{bcrypt}$2a$11$nprb0swJhHitcdRk1N/dRurfZYKneFZ9PGaLW5Np8xNotPlJQ0hTq', 'admin');
INSERT INTO app_user (id, first_name, last_name, password, username) VALUES (3, 'Mod', 'mod', '{bcrypt}$2a$11$nprb0swJhHitcdRk1N/dRurfZYKneFZ9PGaLW5Np8xNotPlJQ0hTq', 'mod');


INSERT INTO user_role(user_id, role_id) VALUES(1,1);
INSERT INTO user_role(user_id, role_id) VALUES(2,1);
INSERT INTO user_role(user_id, role_id) VALUES(2,2);
INSERT INTO user_role(user_id, role_id) VALUES(2,3);
INSERT INTO user_role(user_id, role_id) VALUES(3,3);
INSERT INTO user_role(user_id, role_id) VALUES(3,1);